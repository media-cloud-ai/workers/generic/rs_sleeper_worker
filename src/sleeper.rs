use mcai_worker_sdk::prelude::*;
use schemars::JsonSchema;
use serde_derive::Deserialize;
use std::{thread::sleep, time::Duration};

#[derive(Debug)]
struct SleeperWorkerContext {}

#[derive(Debug, Deserialize, JsonSchema)]
struct SleeperWorkerParameters {
    action: Option<String>,
    #[allow(dead_code)]
    source_path: Option<String>,
    #[allow(dead_code)]
    destination_path: Option<String>,
    /// Option sleep time in milliseconds
    ///
    /// For not media, it will sleep until a stop is received
    ///
    /// For media it will be between each frame
    sleep: Option<u64>,
}

default_rust_mcai_worker_description!();

impl McaiWorker<SleeperWorkerParameters, RustMcaiWorkerDescription> for SleeperWorkerContext {
    fn process(
        &self,
        channel: Option<McaiChannel>,
        parameters: SleeperWorkerParameters,
        job_result: JobResult,
    ) -> Result<JobResult> {
        if let Some(duration) = parameters.sleep {
            let total = 10_u16;
            for count in 0..total {
                if Self::is_current_job_stopped(&channel) {
                    log::info!("Job aborted !!! {}", job_result.get_str_job_id());
                    return Ok(job_result.with_status(JobStatus::Stopped));
                }
                log::info!(
                    "sleep more ({}/{} {})...",
                    count,
                    total,
                    job_result.get_job_id()
                );
                sleep(Duration::from_millis(duration));
                publish_job_progression(
                    channel.clone(),
                    job_result.get_job_id(),
                    ((count + 1) * 100 / total) as u8,
                )?;
            }
        } else {
            publish_job_progression(channel.clone(), job_result.get_job_id(), 50)?;
        }

        match parameters.action {
            Some(action_label) => match action_label.as_str() {
                "completed" => Ok(job_result.with_status(JobStatus::Completed)),
                action_label => {
                    let result =
                        job_result.with_message(&format!("Unknown action named {}", action_label));
                    Err(MessageError::ProcessingError(result))
                }
            },
            None => {
                let result = job_result.with_message("Unspecified action parameter");
                Err(MessageError::ProcessingError(result))
            }
        }
    }
}

fn main() {
    let sleeper_worker_context = SleeperWorkerContext {};
    start_worker(sleeper_worker_context);
}
