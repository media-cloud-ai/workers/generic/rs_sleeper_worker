FROM rust:1.64-buster as builder

ADD . /src
WORKDIR /src

RUN apt-get update && \
    apt-get install -y \
        libssl-dev \
        && \
    cargo build --verbose --release && \
    cargo install --path .

FROM debian:buster
COPY --from=builder /usr/local/cargo/bin/transfer_worker /usr/bin

ENV AMQP_QUEUE job_sleeper
CMD sleeper_worker
